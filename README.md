# myVue

> 基于webpack4搭建vue项目



# 基本步骤

## 1、使用vue-cli构建项目

```
$ npm install -g vue-cli
$ vue init webpack my-project
$ cd my-project
$ npm install
$ npm run dev
```
ps：*创建项目过程中，会让你选择各种基本配置，要认真阅读配置，一般我是e2e测试和单元测试不选，eslint选择预设airbnb，然后让他自动构建成功后运行npm i，就不用自己安装了。*

## 2、配置sass

#### 安装依赖

```
npm install sass-loader node-sass -D
```

```
#### vue文件中添加lang

```
<style lang="scss" scoped></style>
```

这里提醒下

    lang =“scss”对应于CSS-superset语法（带大括号和分号）。

    lang =“sass”对应于基于缩进的语法。


