var compression = require('compression') // gzip压缩的中间件，尽可能先加载这个
var express = require('express')
var http = require('http')
var history = require('connect-history-api-fallback') // 当路由模式为history的时候，后端会直接请求地址栏中的文件，这样就会出现找不到的情况，需要这个中间件处理路由
var proxy = require('http-proxy-middleware') // 代理本地请求，实现跨域

var address = 'hangzhou'
var config = {}
var app = express()
var params = process.argv.slice(2)
var httpServer = http.createServer(app)
var proxyTable = { // 代理到不同服务器不同端口的请求
  'hangzhou': {
    'port': 3000,
    'api': 'http://192.168.1.94:8100',
    'fuhao': 'http://192.168.1.94:8093'
  },
  'xian': {
    'port': 8090,
    'api': 'http://47.99.38.28:8100',
    'fuhao': 'http://47.99.38.28:8093'
  }
}

if (params.length && params[0] === 'xian') {
  address = 'xian'
}

app.use(compression()) // 服务器端gzip文件压缩
config = proxyTable[address]
for (var key in config) {
  var pathRewrite = {}
  pathRewrite['^/' + key] = ''
  app.use('/' + key, proxy({ // proxy反向代理
    target: config[key],
    changeOrigin: true,
    pathRewrite
  }))
}

app.use(history({ // 处理vue 单页面history路由，当请求满足以下条件的时候，该库会把请求地址转到默认（默认情况为index.html）
  index: './index.html'
}))

app.use(express.static('dist')) // express.static: 内置中间件函数,指定资产的目录
httpServer.listen(config.port, (err) => {
  if (err) {
    console.log(err)
    return
  }
  console.log('your server is running at http://localhost:' + config.port + '\n')
})
