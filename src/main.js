import Vue from 'vue'
import App from './App'
import store from './store'
import ElementUI from 'element-ui'
import axios from '@/assets/js/utils/axios.js'
import messageService from '@/assets/js/service/messageservice.js'
import 'element-ui/lib/theme-chalk/index.css' // element-ui样式
import router from './router/index'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$axios = axios
Vue.prototype.$messageService = messageService
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
