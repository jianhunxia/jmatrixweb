import Vue from 'vue'
import Router from 'vue-router'
import storage from '@/assets/js/service/cacheservice'
const Layout = () =>
  import('@/components/layout/Layout')

const staticRoute = [{
  path: '/',
  component: Layout,
  redirect: '/login',
  children: [
    {
      name: 'overview',
      path: 'overview',
      meta: {
        title: '概览',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/overview')
    },
    {
      name: 'configuration',
      path: 'configuration',
      meta: {
        title: '配置包',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/configuration')
    },
    {
      name: 'function',
      path: 'function',
      meta: {
        title: '功能模型',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/function')
    },
    {
      name: 'data',
      path: 'data',
      meta: {
        title: '数据模型',
        requireAuth: true
      },
      component: () =>
        import('@/pages/data')
    },
    {
      name: 'secondGuide',
      path: 'secondGuide',
      meta: {
        title: '向导第二步',
        requireAuth: true
      },
      component: () =>
        import('@/pages/dataEdit/secondGuide')
    },
    {
      name: 'guide',
      path: 'guide',
      meta: {
        title: '向导菜单',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/guide')
    },
    {
      name: 'functionDetail',
      path: 'functionDetail',
      meta: {
        title: '功能模型',
        requireAuth: true
      },
      component: () =>
        import('@/pages/functionDetail/functionDetail')
    },
    {
      name: 'ui',
      path: 'ui',
      meta: {
        title: 'UI模型',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/ui')
    },
    {
      name: 'computeDetails',
      path: 'computeDetails',
      meta: {
        title: '计算模型新',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/computeDetails')
    },
    {
      name: 'menuModel',
      path: 'menu',
      meta: {
        title: '菜单模型',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/menu')
    },
    {
      name: 'name',
      path: 'name',
      meta: {
        title: '显示名称',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/name')
    },
    {
      name: 'dictionary',
      path: 'dictionary',
      meta: {
        title: '字段字典',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/dictionary')
    },
    {
      name: 'list',
      path: 'list',
      meta: {
        title: '列表模型',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/list')
    },
    {
      name: 'spmodel',
      path: 'spmodel',
      meta: {
        title: '存储模型',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/spmodel')
    },
    {
      name: 'baseSetting',
      path: 'baseSetting',
      meta: {
        title: '基础设置',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/baseSetting/baseSetting')
    },
    {
      name: 'importModel',
      path: 'importModel',
      meta: {
        title: '模型导入',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/importModel/importModel')
    },
    {
      name: 'dataBase',
      path: 'dataBase',
      meta: {
        title: '数据库工具',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/dataBase')
    },
    {
      name: 'api',
      path: 'api',
      meta: {
        title: 'api调试',
        requiresAuth: true
      },
      component: () =>
        import('@/pages/api')
    }
  ]
},
{
  name: 'my',
  path: '/my',
  redirect: '/my/service',
  meta: {
    title: '我的账户',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/my'),
  children: [{
    name: 'service',
    path: 'service',
    meta: {
      title: '我的服务',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/service')
  },
  {
    name: 'bill',
    path: 'bill',
    meta: {
      title: '云服务账单',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/bill')
  },
  {
    name: 'serviceDetail',
    path: 'serviceDetail',
    meta: {
      title: '云服务使用明细',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/serviceDetail')
  },
  {
    name: 'modulePubHistory',
    path: 'modulePubHistory',
    meta: {
      title: '模块购买记录',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/modulePubHistory')
  },
  {
    name: 'rechargeInvoice',
    path: 'rechargeInvoice',
    meta: {
      title: '充值与发票',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/rechargeInvoice')
  },
  {
    name: 'setting',
    path: 'setting',
    meta: {
      title: '个人设置',
      requiresAuth: true
    },
    component: () =>
      import('@/pages/my/setting')
  }
  ]
},
{
  name: 'document',
  path: '/document',
  meta: {
    title: '文档中心',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/document')
},
{
  name: 'community',
  path: '/community',
  meta: {
    title: '在线社区',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/community')
},
{
  name: 'moduleStore',
  path: '/moduleStore',
  meta: {
    title: '模块store',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/moduleStore')
},
{
  name: 'team',
  path: '/team',
  meta: {
    title: '团队协作',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/team')
},
{
  name: 'vip',
  path: '/vip',
  meta: {
    title: 'vip服务',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/vip')
},
{
  name: 'order',
  path: '/order',
  meta: {
    title: 'APP订制',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/order')
},
{
  name: 'moduleManage',
  path: '/moduleManage',
  meta: {
    title: '模块管理',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/moduleManage')
},
{
  name: 'selectTemplate',
  path: '/template',
  meta: {
    title: '选择模板',
    requireAuth: true
  },
  component: () => import('@/pages/ui/template')
},
{
  name: 'uiDesign',
  path: '/uiDesign',
  meta: {
    title: 'UI模型设计',
    requireAuth: true
  },
  component: () => import('@/pages/uiDesign')
},
{
  name: 'console',
  path: '/console',
  meta: {
    title: '控制台',
    requiresAuth: true
  },
  component: () =>
    import('@/pages/console')
},
{
  name: 'login',
  path: '/login',
  meta: {
    title: '登陆'
  },
  component: () =>
    import('@/pages/Login')
},
{
  name: 'register',
  path: '/register',
  meta: {
    title: '注册'
  },
  component: () =>
    import('@/pages/register')
},
{
  name: 'forgetPassword',
  path: '/forgetPassword',
  meta: {
    title: '忘记密码'
  },
  component: () =>
    import('@/pages/forgetPassword')
},
{
  path: '*',
  redirect: '/login'
  // component: () => import('@/pages/console')
}
]
// 1、路由初始化
Vue.use(Router)
const redirectRouter = [{
  path: '/',
  redirect: {
    name: 'console'
  }
}]
let router = new Router({
  // mode: 'history', // 可以设置路由模式
  // base: '/dist/', // 生产环境配置
  routes: [
    ...redirectRouter,
    ...staticRoute
  ]
})

// 2、在路由生命周期里验证用户登录的token
router.beforeEach((to, from, next) => {
  // console.log(to, '来源', from)
  if (!to.meta.requiresAuth) { // 是否需要鉴权
    next()
  } else {
    // 判断该路由是否需要登录权限
    storage.getItem('token').then((token) => {
      // console.log(token)
      if (token) {
        next()
      } else {
        next({
          path: '/login',
          query: {
            redirect: to.fullPath
          } // 将跳转的路由path作为参数，登录成功后跳转到该路由
        })
      }
    })
  }
})

export default router
