export default {
  sortByLength: (obj1, obj2) => {
    if (obj1.displayName.length > obj2.displayName.length) {
      return -1
    } else if (obj1.displayName.length < obj2.displayName.length) {
      return 1
    } else {
      return 0
    }
  },
  insertByIndex: (str, newStr, index) => {
    let tempStr = ''
    const pre = str.substr(0, index)
    const after = str.substr(index, str.length)
    tempStr = pre + newStr + after
    return tempStr
  }
}
