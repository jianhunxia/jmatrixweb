import Vue from 'vue'
import Vuex from 'vuex'
import user from './user' // 用户相关
import models from './models' // 模型相关
import options from './options' // 模型相关
import * as types from './types'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  state: {
    string: '昨天',
    token: ''
  },
  mutations: {
    [types.LOGIN] (state, data) {
      localStorage.token = data
      state.token = data
    },
    [types.LOGOUT] (state) {
      localStorage.removeItem('token')
      state.token = null
    },
    [types.TITLE] (state, data) {
      state.title = data
    }
  },
  modules: {
    user,
    models,
    options
  },
  strict: debug, // 开发模式开启严格模式
  plugins: debug ? [createLogger()] : []
})
