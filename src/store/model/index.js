import * as types from '../types'

// state
const state = {
  appInfo: {
    appName: '',
    appVersion: '1.0'
  }
}

// getters
const getters = {
  userInfo: state => {
    return state.userInfo
  }
}

// actions
const actions = {
  setUserInfo (context, payload) {
    context.commit([types.SETAPPINFO], payload)
  }
}

// mutations
const mutations = {
  [types.SETAPPINFO] (state, payload) {
    state.userInfo = payload.userInfo
    state.activeIndex = payload.activeIndex
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
