/* 使用常量 */
const change = 'change'

// state
const state = {
  activeIndex: '/user'
}

// getters
const getters = {
  userInfo: state => {
    return state.userInfo
  }
}

// actions
const actions = {
  setUserInfo (context, payload) {
    context.commit(change, payload)
  }
}

// mutations
const mutations = {
  change (state, payload) {
    state.userInfo = payload.userInfo
    state.activeIndex = payload.activeIndex
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
