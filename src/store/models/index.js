// state
const models = {
  namespaced: true,
  state: {
    uiData: {
      mid: 0, // 主键 可选
      name: '', // 名称
      displayName: '', // 中文名称
      displayNameFormat: '',
      template: '', // 模板
      functionModel: '', // 关联功能
      modelStyle: '', // 模型类型
      description: '', // 描述
      mode: 'PC', // 显示模式 PC/MOBILE
      labelWidth: 90, // label宽度
      labelPosition: 'left', // label的位置
      gutter: 10, // 栅格间隔，单位px
      layoutType: '', // 布局方式可选flex
      justify: '', // flex布局下水平排列方式start/end/center/space-around/space-between
      align: '', // flex布局下垂直排列方式top/middle/bottom
      openConfigs: ['base'], // 展开的配置组
      useCondition: '', // 使用条件，可控制不同客户使用不同page
      customStyle: '', // 自定义样式
      children: [] // 下面的区组
    }
  },
  getters: {

  },
  actions: {
    setUiData (context, payload) {
      context.commit('setUiData', payload)
    }
  },
  mutations: {
    setUiData (state, payload) {
      state.uiData = payload
    }
  }
}

export default models
