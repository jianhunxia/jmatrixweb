import ModelService from '../../assets/js/service/modelservice.js'
import * as types from '../types'

const options = {
  namespaced: true,
  state: {
    fieldKind: [],
    jumpModes: [],
    dataKind: [],
    showType: [],
    dataCategory: [],
    optionsLink: [],
    sourceFunc: [],
    mulitiType: [],
    relationModel: [],
    dataDirct: [],
    linkType: [],
    defineTableIndex: [],
    dataModel: [],
    relationTable: [],
    deploymodule: [],
    dataValidateType: [],
    dataSetList: [],
    getTableList: [],
    getTableNameList: [],
    formatType: []
  },
  actions: {
    async getFieldKind (context) {
      const res = await ModelService.getFieldKind()
      context.commit([types.UPDTAE_OPTIONS], { key: 'fieldKind', list: res.data })
    },
    async getJumpModes (context) {
      const res = await ModelService.getJumpModel()
      context.commit([types.UPDTAE_OPTIONS], { key: 'jumpModes', list: res.data })
    },
    async getDataKind (context) {
      const res = await ModelService.getDataKind()
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataKind', list: res.data })
    },
    async getShowType (context) {
      const res = await ModelService.getShowType()
      context.commit([types.UPDTAE_OPTIONS], { key: 'showType', list: res.data })
    },
    async getDataCategory (context) {
      const res = await ModelService.getDataCategory()
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataCategory', list: res.data })
    },
    async getOptionsLink (context) {
      const res = await ModelService.getOptionsLink()
      context.commit([types.UPDTAE_OPTIONS], { key: 'optionsLink', list: res.data })
    },
    async getSourceFunc (context) {
      const res = await ModelService.getSourceFunc()
      context.commit([types.UPDTAE_OPTIONS], { key: 'sourceFunc', list: res.data })
    },
    async getMulitiType (context) {
      const res = await ModelService.getMulitiType()
      context.commit([types.UPDTAE_OPTIONS], { key: 'mulitiType', list: res.data })
    },
    async getRelationModel (context) {
      const res = await ModelService.getRelationModel()
      context.commit([types.UPDTAE_OPTIONS], { key: 'relationModel', list: res.data })
    },
    async getDataDirct (context) {
      const res = await ModelService.getDataDirct()
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataDirct', list: res.data })
    },
    async getLinkType (context) {
      const res = await ModelService.getLinkType()
      context.commit([types.UPDTAE_OPTIONS], { key: 'linkType', list: res.data })
    },
    async getDefineTableIndex (context, payload) {
      const res = await ModelService.getDefineTableIndex({}, payload)
      context.commit([types.UPDTAE_OPTIONS], { key: 'defineTableIndex', list: res.data })
    },
    async getDataModel (context) {
      const res = await ModelService.getDataModel()
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataModel', list: res.data })
    },
    async getRelationTable (context, payload) {
      const res = await ModelService.getRelationTable({}, payload)
      context.commit([types.UPDTAE_OPTIONS], { key: 'relationTable', list: res.data })
    },
    async getDataSet (context, payload) {
      const res = await ModelService.getDataSet({}, payload)
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataSetList', list: res.data })
    },
    async getTable (context, payload) {
      const res = await ModelService.getTable({}, payload)
      console.log('res', res)
      context.commit([types.UPDTAE_OPTIONS], { key: 'getTableList', list: res.data, isunshift: true })
    },
    async getTableName (context, payload) {
      const res = await ModelService.getTableName({}, payload)
      context.commit([types.UPDTAE_OPTIONS], { key: 'getTableNameList', list: res.data })
    },
    async getDeploymodule (context) {
      const res = await ModelService.getDeploymodule()
      context.commit([types.UPDTAE_OPTIONS], { key: 'deploymodule', list: res.data })
    },
    async getDataValidateType (context) {
      const res = await ModelService.getDataValidateType()
      context.commit([types.UPDTAE_OPTIONS], { key: 'dataValidateType', list: res.data })
    },
    async getformatType (context) {
      const res = await ModelService.getformatType()
      context.commit([types.UPDTAE_OPTIONS], { key: 'formatType', list: res.data })
    }
  },
  mutations: {
    [types.UPDTAE_OPTIONS] (state, payload) {
      // const empty = { key: '', name: '', value: '无' }
      if (!payload.list) payload.list = []
      // if (!payload.isunshift && payload.key !== 'dataKind' && payload.key !== 'fieldKind') payload.list.unshift(empty)
      state[payload.key] = payload.list
    }
  }
}

export default options
