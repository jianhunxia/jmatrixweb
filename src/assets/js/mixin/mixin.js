// 分页操作
export const pageCommon = {
  data () {
    return {
      pageSizeArray: [10, 20, 50],
      pageNo: 1,
      pageSize: 10,
      pageTotal: 0
    }
  },
  created () {
    // console.log(this.creat)
    // nocreat 是否创建时请求
    // 创建不请求 在自己的页面的data设置nocreat字段为true
    if (this.nocreat) {
      console.log(this.nocreat)
    } else {
      this.getList()
    }
  },
  watch: {
    pageSize (newVal, oldVal) {
      if (this.pageTotal > newVal) {
        this.getList()
        return false
      }
      if (this.pageTotal < oldVal) {
        return false
      }
      this.getList()
    }
  },
  methods: {
    // 修改每页显示的数据数目
    handleSizeChange (val) {
      this.pageSize = val
      this.getList()
    },
    // 设置pageNo
    handleCurrentChange (val) {
      this.pageNo = val
      this.getList()
    }
  }
}
