import {
  Notification
} from 'element-ui'
/**
 * SaaS平台的缓存管理服务
 */
class MessageService {
  // Iterate over all items stored in database.
  /**
   *@param {Object} apiconfig API配置对象
   */
  constructor (apiconfig) {
    this.status = true
  }
  success (msg, duration = 2000, title = '成功') {
    return Notification({
      title,
      duration,
      message: msg,
      type: 'success'
    })
  }
  warning (msg, duration = 2000, title = '警告') {
    return Notification({
      title,
      duration,
      message: msg,
      type: 'warning'
    })
  }
  error (msg, duration = 2000, title = '错误') {
    if (this.status) {
      this.status = false
      return Notification({
        title,
        duration,
        message: msg,
        type: 'error',
        onClose: () => {
          this.status = true
        }
      })
    }
  }
  info (msg, duration = 2000, title = '消息') {
    return Notification({
      title,
      duration,
      message: msg,
      type: 'info'
    })
  }
  custom (msgObj) { // 自定义消息类型
    return Notification(msgObj)
  }
  close () {
    // console.log('消息服务close', Notification())
    return Notification().close()
  }

  /**
   * 单例模式取得实例
   */
  static getInstance () {
    if (!MessageService.instance) {
      MessageService.instance = new MessageService()
    }
    return MessageService.instance
  }
}

export default MessageService.getInstance()
