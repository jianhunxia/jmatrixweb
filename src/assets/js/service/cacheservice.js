import localforage from 'localforage'

/**
 * SaaS平台的缓存管理服务
 */
class CacheService {
  // Iterate over all items stored in database.
  iterate (iterator, callback) {
    return localforage.iterate(iterator, callback)
  }
  getItem (key, callback) {
    return localforage.getItem(key, callback)
  }

  setItem (key, value, callback) {
    return localforage.setItem(key, value, callback)
  }

  removeItem (key, callback) {
    return localforage.removeItem(key, callback)
  }

  clear (callback) {
    return localforage.clear(callback)
  }

  length (callback) {
    return localforage.length(callback)
  }

  key (n, callback) {
    return localforage.key(n, callback)
  }
  keys (callback) {
    return localforage.keys(callback)
  }

  /**
   * 单例模式取得实例
   */
  static getInstance () {
    if (!CacheService.instance) {
      CacheService.instance = new CacheService()
    }
    return CacheService.instance
  }
}

export default CacheService.getInstance()
