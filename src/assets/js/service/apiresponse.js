class APIResponse {
  constructor () {
    this.retCode = ''
    this.retMsg = ''
    this.data = null
  }

  /** 单例模式取得实例
   *@description 通过类来执行静态方法获取
   */
  static getInstance () {
    if (!APIResponse.instance) {
      APIResponse.instance = new APIResponse()
    }
    return APIResponse.instance
  }
}

export default APIResponse.getInstance()
