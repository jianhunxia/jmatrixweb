import axios from '../utils/axios'

/** service的基础父类 */
class ServiceBase {
  /**
   *@param {Object} apiconfig API配置对象
   */
  constructor (apiconfig) {
    if (apiconfig !== undefined) {
      ServiceBase.loadExtendAPI(apiconfig) // base内先把 apiconfig内的配置的方法创建到class上
    }
  }

  /** 取得api的url
   * @param {String} kind 模型类型
   * @param {methodName} methodName 方法名
   * @param {methodName} methodName 方法名
   * @description 私有方法，只能在类内部调用
   */
  _getAPIUrlByKind (methodName, string) {
    // console.log('参数', methodName, string)
    const appInfo = JSON.parse(localStorage.appInfo)
    const arr = methodName.split('/')
    if (string) {
      if (arr[0] === 'dbStructureMng') {
        return '/' + methodName + '/' + string
      }
      return '/model/' + appInfo.appName + '/' + appInfo.appVersion + '/' + methodName + '/' + string
    } else {
      if (arr[0] === 'dbStructureMng') {
        return '/' + methodName
      }
      return '/model/' + appInfo.appName + '/' + appInfo.appVersion + '/' + methodName
    }
  }

  /** 示例：根据模型名称取得模型实例
   * @param {ModelKind} kind 类型
   * @param {long Integer} mid 模型的id
   * @returns {APIResponse} 返回结果
   */
  // getModel (kind, modelname) {
  //   // Todo: 先从cache里取,然后再从后端API取，并且存入cache
  //   // baseurl/functionmodel/get
  //   const url = this._getAPIUrlByKind(kind, 'get')
  //   const {appName, appVersion} = this
  //   let cacheObj = storage.getItem(kind + modelname)
  //   let obj
  //   if (cacheObj === undefined) {
  //     obj = axios({
  //       url,
  //       appName,
  //       appVersion
  //     })
  //     if (obj.retCode === 200) {
  //       storage.setItem(kind + modelname, obj.data)
  //     }
  //   } else {
  //     obj = {
  //       retCode: '1',
  //       retMsg: '',
  //       data: cacheObj
  //     }
  //   }
  //   return obj
  //   // 可以加入log记录等等
  // }

  /** 动态创建API方法加到当前对象上
   *@param {Object} apiconfig API置配对象
   *@requires axios path：'../utils/axios'
   */
  static loadExtendAPI (apiconfig) {
    let that = this.prototype
    for (let methodName in apiconfig) { // 取出所有模块 model:模块
      that[methodName] = function (payload, string) { // payload标准参数  string 拼接到url的参数
        // let url = apiconfig[model][name].url
        let url = that._getAPIUrlByKind(apiconfig[methodName].url, string)
        let method = apiconfig[methodName].method
        if (method === 'get') { // 根据请求方式不同，确定传参方式
          let params = {}
          // for (let parameter in that[methodName].params) {
          //   params[parameter] = payload[parameter]
          // }
          params = payload
          return axios({
            url,
            method,
            params
          })
        } else {
          let data = {}
          // for (let parameter in that[methodName].params) {
          //   data[parameter] = payload[parameter]
          // }
          data = payload
          return axios({
            url,
            method,
            data
          })
        }
      }
    }
  }
}

export default ServiceBase
