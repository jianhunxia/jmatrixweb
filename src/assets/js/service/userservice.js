import ServiceBase from './servicebase'
import axios from '../utils/axios'
import storage from './cacheservice'
import apiconfig from '@/assets/js/config/modelserviceapi'
import router from '@/router'
/** SaaS平台的用户管理服务
 *@param {Object} apiconfig API置配对象
 *@requires axios path：'../utils/axios'
 *@author guotongqing
 */
class UserService extends ServiceBase {
  constructor () {
    super(apiconfig)
  }

  /** 单例模式取得实例
   *@description 通过类来执行静态方法获取
   */
  static getInstance () {
    if (!UserService.instance) {
      UserService.instance = new UserService()
    }
    return UserService.instance
  }

  /** 系统登录
   *@param {Number} account 账号
   *@param {String} pwd 密码
   *@return {Object} 用户信息
   */
  // 注册验证账号唯一
  validateAccount (account) {
    let url = 'user/validateAccount'
    axios({
      url,
      method: 'get',
      params: {
        account
      }
    }).then((res) => {
      console.log('res', res)
    })
  }

  register (account, pwd) {
    let url = '/user/add'
    axios({
      url,
      method: 'post',
      data: {
        account,
        pwd
      }
    }).then((res) => {
      console.log('res', res)
    })
  }

  /** 系统登录
   *@param {Number} account 账号
   *@param {String} pwd 密码
   *@return {Object} 用户信息
   */
  login (account, password, keepLogin) {
    let url = '/user/login'
    let index = 0
    axios({
      url,
      method: 'post',
      data: {
        account,
        password,
        keepLogin
      }
    }).then((res) => {
      storage.setItem('userInfo', {
        username: res.data.account,
        userId: res.data.id,
        gender: res.data.gender,
        password: res.data.password,
        telephone: res.data.telephone,
        name: res.data.username,
        lastLoginTime: res.data.lastLoginTime,
        userImg: 'http://img.zcool.cn/community/012cce5a587491a80120121f1e67e9.jpg@2o.jpg',
        userEmail: res.data.email
      })
      storage.setItem('token', res.data.token, () => {
        router.push('/console')
      })
      if (!keepLogin) {
        storage.removeItem('user')
      }
    }).catch(err => {
      console.log(err)
      index++
      console.log(index)
    })
  }

  /**
   * 校验验证码
   */
  async checkCode (params) {
    const url = '/captcha/validate'
    let data = {}
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  /**
   * 退出系统
   */
  logout () {
    let url = '/user/logout'
    axios({
      url,
      method: 'get'
    }).then(async (res) => {
      await storage.removeItem('token')
      await storage.removeItem('userInfo')
      storage.removeItem('appInfo').then(() => {
        router.push('/login')
      })
    }).catch((err) => {
      console.log(err)
    })
  }

  // 账户中心 修改用户信息
  async updataInfo (params) {
    const url = '/user/updateUserInfo'
    let data = {}
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  /**
   * 修改密码,返回修改状态
   */
  // changePassword () {
  //   return false
  // }

  haslogin () {
    return false
  }

  // 新增产品-获取appID 和 selectID
  async getId () {
    const url = '/model/appmodel/generatecode'
    let data = {}
    await axios({
      url,
      method: 'get',
      params: null
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 产品排序
  async sortFct (params) {
    const url = '/model/appmodel/sortmodel'
    let data = {}
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 获取产品详情
  async getInfo (appName, appVersion) {
    console.log(appName, appVersion)
    // var getTimestamp = new Date().getTime()
    const url = '/model/appmodel' + '/' + appName + '/' + appVersion
    let data = {}
    await axios({
      url,
      method: 'get',
      params: {
        appName,
        appVersion
      }
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 新增产品-获取appID 和 selectID
  addProduct (params, fct) {
    let url = 'model/appmodel/save'
    axios({
      url,
      method: 'post',
      params: params
    }).then((res) => {
      fct()
    })
  }
  // 新增产品-获取appID 和 selectID
  async checkName (name, mid, appVersion) {
    const url = 'model/appmodel/updatemodelname'
    let data = {}
    const params = {
      name,
      mid,
      appVersion
    }
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 新增版本
  async addVersionFct (params) {
    const url = '/model/appversion/save'
    let data = {}
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 获取版本信息列表
  async getVersionList (params) {
    let data = {}
    await axios.get('model/appversion/list/' + params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 获取功能模型基础数据
  async getDefaultFct (appName, appVersion) {
    let data = {}
    await axios.get('model/' + appName + '/' + appVersion + '/functionmodel/getbaseinfo/').then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 获取权限行为
  async getpermissionpoints (appName, appVersion, functionName) {
    const url = 'model/' + appName + '/' + appVersion + '/functionmodel/getpermissionpoints'
    let data = {}
    const params = {
      appName,
      appVersion,
      functionName
    }
    await axios.get(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 新增权限行为
  async addPointFct (appName, appVersion, functionName, permissionName, permissionValue) {
    let data = {}
    const params = {
      appName,
      appVersion,
      functionName,
      permissionName,
      permissionValue
    }
    let url = 'model/' + appName + '/' + appVersion + '/functionmodel/addpermission'
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 删除权限行为
  async deletePonitFct (appName, appVersion, functionName, permissionName, permissionValue) {
    let data = {}
    const params = {
      appName,
      appVersion,
      functionName,
      permissionName,
      permissionValue
    }
    let url = 'model/' + appName + '/' + appVersion + '/functionmodel/deletepermission'
    await axios.post(url, params).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
  // 获取功能模型差异
  async getdiffmessageFct (appName, appVersion, modelName) {
    let data = {}
    await axios.get('model/' + appName + '/' + appVersion + '/functionmodel/getdiffmessage/' + modelName).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 发布功能
  async publishfunction (appName, appVersion, modelName) {
    let data = {}
    let url = ''
    if (modelName === 'other') {
      url = 'model/' + appName + '/' + appVersion + '/functionmodel/publishfunctions'
    } else {
      url = 'model/' + appName + '/' + appVersion + '/functionmodel/publishfunction/' + modelName
    }
    await axios.get(url).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 获取模型导入列表
  async geImportList (appName, appVersion, pageNo, pageSize, name, oldVersion) {
    let data = {}
    let url = ''
    url = 'model/' + appName + '/' + appVersion + '/modelimport/page?' + 'pageNo=' + pageNo + '&pageSize=' + pageSize + '&name=' + name + '&oldVersion=' + oldVersion
    await axios.get(url).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 模型导入接口
  async setImport (appName, appVersion, modelList) {
    let data = {}
    let url = ''
    url = 'model/' + appName + '/' + appVersion + '/modelimport/import'
    await axios.post(url, modelList).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 全部导入
  async setImportAll (appName, appVersion) {
    let data = {}
    let url = ''
    url = 'model/' + appName + '/' + appVersion + '/modelimport/importall'
    await axios.get(url).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 模型提交
  async submitFunction (appName, appVersion, params) {
    let data = {}
    let url = ''
    let param = params
    url = 'model/' + appName + '/' + appVersion + '/functionmodel/commit'
    await axios.post(url, param).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  // 获取功能模型编辑
  async getFunctionDetail (appName, appVersion, name) {
    let data = {}
    let url = ''
    url = 'model/' + appName + '/' + appVersion + '/functionmodel/get/' + name
    await axios.get(url).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
}

export default UserService.getInstance()
