import ServiceBase from './servicebase'
import modelserviceapi from '../config/modelserviceapi'
// import storage from './cacheservice'
import axios from '../utils/axios'

/**
 * SaaS平台的模型业务领域服务
 *@param {Object} apiconfig API置配对象
 *@requires axios path：'../utils/axios'
 */
class ModelService extends ServiceBase {
  constructor (apiconfig) {
    super(apiconfig)
    this.appInfo = {}
  }

  /** 单例模式取得实例
   *@description 通过类来执行静态方法获取
   */
  static getInstance () {
    if (!ModelService.instance) {
      ModelService.instance = new ModelService(modelserviceapi)
    }
    return ModelService.instance
  }

  /** 示例：根据模型名称取得模型实例
   * @param {ModelKind} kind 类型
   * @param {long Integer} mid 模型的id
   * @returns {APIResponse} 返回结果
   */
  _getAPIUrlByKind (kind, methodName) {
    if (kind === 'appmodel') {
      return '/model/' + kind + '/' + methodName
    } else {
      if (!this.appInfo.appName) {
        const appInfo = JSON.parse(localStorage.appInfo)
        return '/model/' + appInfo.appName + '/' + appInfo.appVersion + '/' + kind + '/' + methodName
      } else {
        return '/model/' + this.appInfo.appName + '/' + this.appInfo.appVersion + '/' + kind + '/' + methodName
      }
    }
  }

  /**
   * 新增或者修改模型
   * @param {ModelKind} kind 类型
   * @param {Object} modelObj 要保存的模型实例
   * @returns APIResponse
   */
  async saveModel (kind, modelObj) {
    const url = this._getAPIUrlByKind(kind, 'save')
    let data = {}
    await axios({
      url,
      method: 'post',
      data: modelObj
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  /**
   * 根据模型id删除模型
   * @param {String} kind 模型类型
   * @param {String} mid 模型的编号
   * @returns APIResponse
   */
  async deleteModel (kind, mid) {
    let methodName = 'delete' + '/' + mid
    const url = this._getAPIUrlByKind(kind, methodName)
    let data = {}
    await axios({
      url,
      method: 'get'
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  /**
   * 获取模型信息
   * @param {String} kind 模型类型
   * @param {String} modelName 模型的名称
   * @returns 模型详情
   */
  async getModelInfo (kind, modelName, status) {
    let methodName = status ? 'get' + '/' + modelName + '/' + status : 'get' + '/' + modelName
    const url = this._getAPIUrlByKind(kind, methodName)
    let data = {}
    await axios({
      url,
      method: 'get',
      data: {
        name
      }
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  /**
   * 模糊查询模型列表
   * @param {String} kind 模型类型
   * @param {String} nameCondition 名称模糊查询条件
   * @return list 查询列表
   */
  async quickQuery (kind, nameCondition) {
    const url = this._getAPIUrlByKind(kind, 'quickquery')
    let data = {}
    await axios({
      url,
      method: 'get',
      params: {
        nameCondition
      }
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  /**
   * 快速条件分页查询模型列表
   * @param {ModelKind} kind 模型类型
   * @param {Integer} pageNo 页码 默认1
   * @param {Integer} pageSize 每页数量 默认20
   * @param {String} nameCondition 可选参数 模型id
   */
  async queryByPage (kind, pageNo, pageSize, nameCondition = null) {
    const url = this._getAPIUrlByKind(kind, 'querybypage')
    const timestamp = (new Date()).getTime()
    let data = {}
    await axios({
      url,
      method: 'get',
      params: {
        timestamp,
        pageNo,
        pageSize,
        nameCondition
      }
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }

  /**
   * 获取全部模型数据
   * @param {String} kind 类型
   * @param {String} mid 模型id
   */
  async listAll (kind) {
    let data = {}
    const url = this._getAPIUrlByKind(kind, 'list')
    await axios({
      url,
      method: 'get'
    }).then((res) => {
      data = res
    })
    return Promise.resolve(data)
  }
}
const obj = ModelService.getInstance()
export default obj
