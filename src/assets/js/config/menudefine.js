let menu = [ // 侧边栏menu数据
  {
    title: '模型平台'
  },
  {
    icon: 'icon-mokuaiguanli',
    path: '/configuration',
    title: '配置包'
  },
  // {
  //   icon: 'icon-mingcheng',
  //   path: '/guide',
  //   title: '向导菜单'
  // },
  {
    icon: 'icon-kuaijiecaidan',
    path: '/function',
    title: '功能模型'
  },
  {
    icon: 'icon-jiemianyangshibiaofenleianniugongjulandengfenlei',
    path: '/ui',
    title: 'UI模型'
  },
  {
    icon: 'icon-caidan',
    path: '/menu',
    title: '菜单模型'
  },
  // {
  //   icon: 'icon-jisuanqi',
  //   path: '/compute',
  //   title: '计算模型'
  // },
  // {
  //   icon: 'icon-jisuanqi',
  //   path: '/computeDetails',
  //   title: '计算模型'
  // },
  {
    icon: 'icon-cunchu',
    path: '/spmodel',
    title: '存储过程'
  },
  {
    icon: 'icon-houqiweihuweihuweihuguanli',
    path: '/baseSetting',
    title: '基础设置'
  },
  {
    title: '云工具'
  },
  {
    icon: 'icon-daoru2',
    path: '/importModel',
    title: '模型导入'
  },
  {
    icon: 'icon-tongbushujukumoxing',
    path: '/dataBase',
    title: '数据库工具'
  },
  {
    icon: 'icon-diaoshi',
    path: '/api',
    title: 'API调试'
  }
]
export default menu
