let modelServiceApi = {
  // serviceApi的配置
  'getTemplates': { // ui模型获取模板列表
    method: 'get',
    url: 'uilayoutmodel/getTemplates'
  },
  'getFieldList': { // ui模型获取数据源字段列表
    method: 'get',
    url: 'datasourcemodel'
  },
  'getFunctionDetail': { // 获取功能详情
    method: 'get',
    url: 'functionmodel/getmodel'
  },
  'getFunctionList': { // 获取已保存的功能列表
    method: 'get',
    url: 'functionmodel/listCommitted'
  },
  'submitFun': { // 功能模型提交
    method: 'post',
    url: 'functionmodel/commitfunction'
  },
  'submitCompute': { // 计算模型提交
    method: 'post',
    url: 'linkagemodel/commit'
  },
  'menuTree': { // 获取菜单树
    method: 'get',
    url: 'menumodel/menuTree'
  },
  'sortMenu': {
    method: 'post',
    url: 'menumodel/sortMenu'
  },
  'listSp': { // 获取存储过程下拉列表
    method: 'get',
    url: 'storedProcedureModel/listSp'
  },
  'getTables': { // 获取计算模型字段树
    method: 'get',
    url: 'datasourcemodel/getTables'
  },
  'configTree': { // 获取配置包树
    method: 'get',
    url: 'deploymodulemodel/tree'
  },
  'analyse': { // 数据库工具分析
    method: 'get',
    url: 'dbStructureMng/analyse'
  },
  'upgrade': { // 数据库工具升级
    method: 'post',
    url: 'dbStructureMng/upgrade'
  },
  'createForcibly': { // 数据库工具强制创建
    method: 'get',
    url: 'dbStructureMng/createForcibly'
  },
  'submitModel': { // 提交数据模型， 非保存
    method: 'post',
    url: 'datasourcemodel/commit'
  },
  'getOptionsSet': {
    method: 'get',
    url: 'pickvaluemodel/get'
  },
  'getJumpModel': { // 跳号规则
    method: 'get',
    url: 'datasourcemodel/pick/jump_mode'
  },
  'getFieldKind': { // 字段样式
    method: 'get',
    url: 'datasourcemodel/pick/field_kind'
  },
  'getDataKind': { // 数据类型
    method: 'get',
    url: 'datasourcemodel/pick/data_type'
  },
  'getShowType': { // 控件显示类型
    method: 'get',
    url: 'datasourcemodel/pick/control_type'
  },
  'getDataCategory': { // 数据模型类型
    method: 'get',
    url: 'datasourcemodel/pick/category'
  },
  'getOptionsLink': { // 选项值链接
    method: 'get',
    url: 'pickvaluemodel/listbyname'
  },
  'getSourceFunc': { // 来源功能
    method: 'get',
    url: 'functionmodel/listbyname'
  },
  'getMulitiType': { // 多选类型
    method: 'get',
    url: 'datasourcemodel/pick/multi_sel_type'
  },
  'getRelationModel': { // 联通计算模型
    method: 'get',
    url: 'linkagemodel/listbyname'
  },
  'getDataDirct': { // 数据字典
    method: 'get',
    url: 'fielddictmodel/listbyname'
  },
  'getLinkType': { // 连接方式
    method: 'get',
    url: 'datasourcemodel/pick/join_kind'
  },
  'getDataModel': { // 数据模型名称
    method: 'get',
    url: 'datasourcemodel/listbyname'
  },
  'getDeploymodule': { // 数据模型名称
    method: 'get',
    url: 'deploymodulemodel/listbyname'
  },
  'getDefineTableIndex': { // 定义表主键
    method: 'get',
    url: 'datasourcemodel'
  },
  'getRelationTable': { // 关联表名称
    method: 'get',
    url: 'datasourcemodel'
  },
  'getDataSet': { // 关联数据集下拉
    method: 'get',
    url: 'datasourcemodel'
  },
  'getTable': { // 关联表格
    method: 'get',
    url: 'datasourcemodel'
  },
  'getTableName': { // 关联表格名称
    method: 'get',
    url: 'datasourcemodel'
  },
  'getformatType': { // 格式化类型
    method: 'get',
    url: 'datasourcemodel/pick/number_format_type'
  },
  'getDataValidateType': { // 数字验证类型
    method: 'get',
    url: 'datasourcemodel/pick/number_valid_type'
  },
  'getModelTree': { // 获取数据模型树形数据
    method: 'get',
    url: 'datasourcemodel'
  }
}

export default modelServiceApi
