import axios from 'axios'
import router from '@/router'
import storage from '../service/cacheservice'
// import { Loading } from 'element-ui'
import messageService from '@/assets/js/service/messageservice'

// axios 配置
axios.defaults.timeout = 30000
axios.defaults.baseURL = '/api'

// let loading
// http request 拦截器
axios.interceptors.request.use(async function (config) {
  await storage.getItem('token').then((token) => {
    if (config.url.includes('fuhao')) { // 代理不同的服务
      config.baseURL = ''
      // config.url = config.url.replace('fuhao', 'api2')
    }
    if (token && config.url !== '/user/login' && config.url !== '/user/register' && config.url !== '/user/forgetPassword') {
      config.headers.Authorization = token
    }
  })
  // loading = Loading.service({
  //   lock: true,
  //   spinner: 'el-icon-loading',
  //   customClass: 'loading-custom-name',
  //   text: '加载中……',
  //   background: 'rgba(0, 0, 0, 0.7)'
  // })
  return config
}, err => {
  return Promise.reject(err)
})
// http response 拦截器
axios.interceptors.response.use(res => {
  // loading.close()
  // console.log('http响应res', res)
  if (!res || !res.data) {
    messageService.error('系统暂时不可用，请稍后')
  }
  if (res.data && res.data.code !== 200) {
    if (res.data.message) {
      switch (res.data.code) {
        case 1001:
          messageService.error(res.data.message)
          router.push('/login')
          break
        default:
          messageService.error(res.data.message)
          break
      }
    } else {
      messageService.error('系统未知错误，将返回登录页')
      router.push('/login')
    }
    return Promise.reject(res.data)
  }
  return res.data
}, err => {
  // loading.close()
  console.log('错误', err.response)
  if (err && err.response) {
    switch (err.response.status) {
      case 400:
        err.message = '请求错误(400)'
        break
      case 401:
        err.message = '未授权，请重新登录(401)'
        router.push('/login')
        break
      case 403:
        err.message = '拒绝访问(403)'
        break
      case 404:
        err.message = '请求的资源不存在(404)'
        break
      case 408:
        err.message = '请求超时(408)'
        break
      case 500:
        err.message = '服务器错误(500)'
        break
      case 501:
        err.message = '服务未实现(501)'
        break
      case 502:
        err.message = '网络错误(502)'
        break
      case 503:
        err.message = '服务不可用(503)'
        break
      case 504:
        err.message = '网络超时(504)'
        break
      case 505:
        err.message = 'HTTP版本不受支持(505)'
        break
      default:
        err.message = `连接出错(${err.response.status})!`
    }
  } else {
    err.message = '连接服务器失败!'
  }
  messageService.error(err.message)
  return Promise.reject(err)
})

export default axios
