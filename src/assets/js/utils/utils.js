/** 方法说明
 *@method 方法名
 *@param {参数类型}参数名 参数说明
 *@return {返回值类型} 返回值说明
 *@author 作者
 *@version 版本号
 *@example 示例
 *@{@link} 链接
 *@for 所属类名
 *@namespace 命名空间
 *@requires 依赖模块
 *@exports 标识一个由JavaScript模块导出的成员
 */

/** Javascript生成全局唯一标识符（GUID,UUID）
 *@return {String} 生成的全局唯一标识符
 *@author guotongqing
 */
export function guid () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0 // | 0：表示取整，只保留整数部分
    const v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}

/** Javascript生成全局唯一的key
 *@description  利用时间戳加随机数拼凑出唯一的key
 *@return {String} 生成的全局唯一的key
 */
export function unique () {
  return Date.parse(new Date()) + '_' + Math.ceil(Math.random() * 99999)
}

/** 监测设备屏幕宽度，实时返回所属屏宽类型
 *@return {Number} 屏宽代号
 *@author guotongqing
 */
export function getDeviceType () {
  // var userAgentInfo = navigator.userAgent;
  // var Agents = ['Android', "iPhone","SymbianOS", "Windows Phone", "iPod"];
  // var flag = true;
  // for (var v = 0; v < Agents.length; v++) {
  //   if (userAgentInfo.indexOf(Agents[v]) > 0) {
  //     flag = false;
  //     break;
  //   }
  // }
  const width = window.innerWidth
  let widthType
  if (width >= 1200) widthType = 2
  if (width > 750 && width < 1200) widthType = 1
  if (width <= 750) widthType = 0
  return widthType
}

/** 切换不同的屏宽类型
 *@param {Number} widthType:当前屏幕宽度代号
 *@return {Number} 新的屏宽代号
 *@author guotongqing
 */
export function changeWidthType (widthType) {
  switch (widthType) {
    case 2:
      return 1
    case 1:
      return 2
    case 0:
      return -1
    case -1:
      return 0
    default:
      break
  }
}

/** 根据不同的屏宽为dom添加不同的class名
 *@param {Number} type：屏宽类型代号
 *@param {String} baseName：class名固定的部分
 *@return {String} class名
 *@author guotongqing
 */
export function whichClass (type, baseName) {
  switch (type) {
    case 2:
      return 'max' + baseName
    case 1:
      return 'mid' + baseName
    case 0:
      return 'min' + baseName
    case -1:
      return 'max' + baseName
    default:
      break
  }
}

/** 根据毫秒时间戳和类型得到对应的日期时间
 *@param {Number} tm：毫秒时间戳
 *@param {Number} type: 日期时间类型代号
 *@description type对应的时间类型--0：2018/5/8  1: 时间（下午2：00） 2: 星期几 3: 完整日期  4:时间（20：00）5:周几（1-7） 6:完整时间 7:日期
 *@return {String} 转换好的日期时间
 *@author guotongqing
 */
export function getDate (tm, type) {
  function add0 (m) {
    return m < 10 ? `0${m}` : m
  }

  function date6 (time) {
    let y = time.getFullYear()
    let m = time.getMonth() + 1
    let d = time.getDate()
    let h = time.getHours()
    let mm = time.getMinutes()
    let s = time.getSeconds()
    return `${y}-${add0(m)}-${add0(d)} ${add0(h)}:${add0(mm)}:${add0(s)}`
  }

  function date7 (time) {
    let m = time.getMonth() + 1
    let d = time.getDate()
    let h = time.getHours()
    let mm = time.getMinutes()
    return `${m}/${d} ${add0(h)}:${add0(mm)}`
  }

  function date8 (time) {
    let y = time.getFullYear()
    let m = time.getMonth() + 1
    let d = time.getDate()
    return `${y}-${add0(m)}-${add0(d)}`
  }
  let time = new Date(tm)
  let tt = time.toLocaleString()
  let hour = time.getHours()
  let minute = time.getMinutes()
  if (minute < 10) {
    minute = `0${minute}`
  }
  if (hour < 10) {
    hour = `0${hour}`
  }
  let week = (new Date(tm)).getDay()
  if (type === 2) { // 返回“星期几的样子”
    let date = week
    switch (date) {
      case 0:
        return '星期天'
      case 1:
        return '星期一'
      case 2:
        return '星期二'
      case 3:
        return '星期三'
      case 4:
        return '星期四'
      case 5:
        return '星期五'
      case 6:
        return '星期六'
      default:
        break
    }
  } else if (type === 3) { // 返回日期
    return tt
  } else if (type === 4) { // 返回小时分钟
    return `${hour}:${minute}`
  } else if (type === 5) { // 返回周几
    if (week !== 0) return week
    return 7
  } else if (type === 1 || type === 0) {
    return tt.split(' ')[type]
  } else if (type === 6) {
    return date6(time)
  } else if (type === 7) {
    return date7(time)
  } else if (type === 8) {
    return date8(time)
  }
}

/** 数组/对象深拷贝
 *@param {Object | Array} source：将要拷贝的数据源
 *@param {Number} index: 将要拷贝的数据源如果包含循环引用时，递归会导致死循环，所以index为递归调用的次数，应大于等于数据源的最大重数
 *@description  关于参数index---如source = {arr:[1,2,3]} 是两重复杂数据类型，那index应>=2,默认值为10
 *@return {String} 经过深拷贝的数据
 *@author guotongqing
 */
let num = 0
export function deepClone (source, index = 10) { // 函数5：数组和对象深拷贝通用方法 source：想要拷贝的数据源
  num++
  const targetObj = source.constructor === Array ? [] : {} // 判断复制的目标是数组还是对象
  for (let keys in source) {
    if (source.hasOwnProperty(keys)) {
      if (source[keys] && typeof source[keys] === 'object' && num < index) { // 如果值是对象，就递归一下
        targetObj[keys] = source[keys].constructor === Array ? [] : {}
        targetObj[keys] = deepClone(source[keys])
      } else { // 如果不是，就直接赋值
        targetObj[keys] = source[keys]
      }
    }
  }
  return targetObj
}

/** 数组去重
 *@param {Array} arr:要去重的数组
 *@return {String} 去重后的数组
 *@author guotongqing
 */
export function uniqueArr (arr) {
  var x = new Set(arr)
  return [...x]
}

/** 异步加载JS
 *@param {String} url:js的url
 *@return {Object} 异步请求的promise对象
 */
export function loadJs (url) {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.src = url
    script.type = 'text/javascript'
    document.body.appendChild(script)
    script.onload = () => {
      resolve()
    }
  })
}

/** 异步加载CSS
 *@param {String} url:CSS的url
 *@return {Object} 异步请求的promise对象
 */
export function loadCss (url) {
  return new Promise((resolve, reject) => {
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = url
    document.head.appendChild(link)
    link.onload = () => {
      resolve()
    }
  })
}

/** 身份证校验
 *@param {String} id:身份证号
 *@description  二代身份证规则：前17位是本体码，第十八位为校验码 规则为1-6：省市县 7-14：出生日期 15-16：当地派出所 17：性别男奇数女偶数 18：校验码通过公式得到，公式:将本体码各位数字乘以对应加权因子并求和，除以11得到余数，根据余数通过校验码对照表查得校验码。
 *@return {Boolean} true符合规则，false不符合
 */
export function checkIdNum (id) {
  var regexpress = /^(\d{6})(18|19|20|21)?(\d{2})([01]\d)([0123]\d)(\d{3})(\d|X|x)?$/
  if (regexpress.test(id) === true) {
    var str = id
    var len = str.length // 身份证编码长度
    if ((len !== 15) && (len !== 18)) { // 判断编码位数等于15或18
      return false
    } else {
      // 15位身份证
      if (len === 15) {
        return true
      }
      // 18位身份证 出生年份在1890-2100之间
      if (len === 18) {
        if (str.substr(6, 2) === '18') {
          if (parseInt(str.substr(8, 2)) < 90) {
            return false
          }
        } else if (str.substr(6, 2) === '21') {
          if (parseInt(str.substr(8, 2)) !== '00') {
            return false
          }
        }
        if (str.charAt(len - 1) !== getCheckDigit18(str)) { // 校验第18位的校验码
          return false
        }
      }
    }
    return true
  } else {
    return false
  }
}
function getCheckDigit18 (idCardNo) {
  var VERIFY18RIGHTS = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2] // 前17位加权因子
  var VERIFY18CHECKDIGIT = '10X98765432' // 余数1-10对应的校验码
  var sum = 0
  for (var i = 0; i < VERIFY18RIGHTS.length; i++) { // 前17位加权求和
    sum += parseInt(idCardNo.charAt(i)) * VERIFY18RIGHTS[i]
  }
  return VERIFY18CHECKDIGIT.charAt(sum % 11) // 对加权求和值对11取模，并匹配校验码
}

/** 获取本机ip
 *@param {Function} callback 获取到ip后的回调
 *@description  通过利用WebRTC(Web Real-Time Communications 一项实时通讯技术，它允许网络应用或者站点，在不借助中间媒介的情况下，建立浏览器之间点对点（Peer-to-Peer）的连接，实现视频流和（或）音频流或者其他任意数据的传输)和正则获取本机ip
 */
export function getIP (callback) { //  onNewIp - your listener function for new IPs
  // 兼容firefox和chrome
  var MyPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection
  var pc = new MyPeerConnection({ iceServers: [] })
  var noop = function () { }
  var localIPs = {}
  var ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g

  function iterateIP (ip) {
    if (!localIPs[ip]) callback(ip)
    localIPs[ip] = true
  }

  // 创建一个伪造的数据通道
  pc.createDataChannel('')

  // 创建报价并设置本地描述
  pc.createOffer().then(function (sdp) {
    sdp.sdp.split('\n').forEach(function (line) {
      if (line.indexOf('candidate') < 0) return
      line.match(ipRegex).forEach(iterateIP)
    })
    pc.setLocalDescription(sdp, noop, noop)
  }).catch(function (reason) {
    // 发生错误，请处理连接失败
  })

  // sten用于候选事件
  pc.onicecandidate = function (ice) {
    if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return
    ice.candidate.candidate.match(ipRegex).forEach(iterateIP)
  }
}

/** 获取本机ip
 *@param {Function} callback 获取到ip后的回调
 *@description  通过利用WebRTC(Web Real-Time Communications 一项实时通讯技术，它允许网络应用或者站点，在不借助中间媒介的情况下，建立浏览器之间点对点（Peer-to-Peer）的连接，实现视频流和（或）音频流或者其他任意数据的传输)和正则获取本机ip
 */
export function getLocalIP (callback) {
  var RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection
  if (RTCPeerConnection) {
    (function () {
      var rtc = new RTCPeerConnection({ iceServers: [] })
      if (1 || window.mozRTCPeerConnection) {
        rtc.createDataChannel('', { reliable: false })
      };

      rtc.onicecandidate = function (evt) {
        if (evt.candidate) grepSDP('a=' + evt.candidate.candidate)
      }
      rtc.createOffer(function (offerDesc) {
        grepSDP(offerDesc.sdp)
        rtc.setLocalDescription(offerDesc)
      }, function (e) { console.warn('offer failed', e) })

      var addrs = Object.create(null)
      addrs['0.0.0.0'] = false
      function updateDisplay (newAddr) {
        if (newAddr in addrs) return
        else addrs[newAddr] = true
        var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k] })
        for (var i = 0; i < displayAddrs.length; i++) {
          if (displayAddrs[i].length > 16) {
            displayAddrs.splice(i, 1)
            i--
          }
        }
        callback(displayAddrs)
      }

      function grepSDP (sdp) {
        sdp.split('\r\n').forEach(function (line, index, arr) {
          if (~line.indexOf('a=candidate')) {
            var parts = line.split(' ')
            var addr = parts[4]
            var type = parts[7]
            if (type === 'host') updateDisplay(addr)
          } else if (~line.indexOf('c=')) {
            const parts = line.split(' ')
            const addr = parts[2]
            updateDisplay(addr)
          }
        })
      }
    })()
  } else {
    alert('请使用主流浏览器：chrome,firefox,opera,safari')
  }
}

/** 获取系统和浏览器
 *@description  利用navigator对象获取本机的系统和浏览器的信息
 *@return {Array} arr [系统， 子系统， 浏览器， 浏览器版本]
 */
export function getBrowserInfo () {
  var agent = navigator.userAgent.toLowerCase()
  var arr = []
  var system = agent.split('(')[1].split(')')[0].split(';')[0]
  var systemVersion = agent.split('(')[1].split(')')[0].split(';')[1]
  arr.push(system)
  arr.push(systemVersion)
  var regEdge = /edge\/[\d.]+/gi
  var regIe = /trident\/[\d.]+/gi
  var regFf = /firefox\/[\d.]+/gi
  var regChrome = /chrome\/[\d.]+/gi
  var regSaf = /safari\/[\d.]+/gi
  var regOpera = /opr\/[\d.]+/gi
  // IE
  if (agent.indexOf('trident') > 0) {
    arr.push(agent.match(regIe)[0].split('/')[0])
    arr.push(agent.match(regIe)[0].split('/')[1])
    return arr
  }
  // Edge
  if (agent.indexOf('edge') > 0) {
    arr.push(agent.match(regEdge)[0].split('/')[0])
    arr.push(agent.match(regEdge)[0].split('/')[1])
    return arr
  }
  // firefox
  if (agent.indexOf('firefox') > 0) {
    arr.push(agent.match(regFf)[0].split('/')[0])
    arr.push(agent.match(regFf)[0].split('/')[1])
    return arr
  }
  // Opera
  if (agent.indexOf('opr') > 0) {
    arr.push(agent.match(regOpera)[0].split('/')[0])
    arr.push(agent.match(regOpera)[0].split('/')[1])
    return arr
  }
  // Safari
  if (agent.indexOf('safari') > 0 && agent.indexOf('chrome') < 0) {
    arr.push(agent.match(regSaf)[0].split('/')[0])
    arr.push(agent.match(regSaf)[0].split('/')[1])
    return arr
  }
  // Chrome
  if (agent.indexOf('chrome') > 0) {
    arr.push(agent.match(regChrome)[0].split('/')[0])
    arr.push(agent.match(regChrome)[0].split('/')[1])
    return arr
  } else {
    arr.push('请更换主流浏览器，例如chrome,firefox,opera,safari,IE,Edge!')
    return arr
  }
}

/** 获取公网IP和国家以及语言
 *@description  利用新浪的开放接口查询
 *@return {Object}  {"cip": "192.168.1.1", "cid": "CN", "cname": "CHINA"}
 */
export function getInternetIp () {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.src = 'http://pv.sohu.com/cityjson?ie=utf-8'
    script.type = 'text/javascript'
    document.body.appendChild(script)
    script.onload = (res) => {
      resolve(window.returnCitySN)
    }
  })
}
