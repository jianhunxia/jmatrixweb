import Vue from 'vue'
const bus = new Vue() // 1、vue bus实现组件间通信
const emailReg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/ // 2、邮箱验证正则
const nameReg = /^[a-zA-Z_\.-][a-zA-Z0-9_\.-]*$/ // 3、名称正则：名称只允许为字母数字或者._-且不能数字开头
const chineseReg = /^[\u4e00-\u9fa5]+$/ // 4、中文正则
const numReg = /^[0-9]{1}$/ // 5、数字正则

export {
  emailReg,
  bus,
  nameReg,
  chineseReg,
  numReg
}
