// 权限行为
export const authority = [{
  value: 'Use',
  label: 'Use(使用)',
  text: '使用'
}, {
  value: 'Browse',
  label: 'Browse(查询)',
  text: '查询'
}, {
  value: 'AddNew',
  label: 'AddNew(创建)',
  text: '创建'
}, {
  value: 'Edit',
  label: 'Edit(修改)',
  text: '修改'
}, {
  value: 'Delete',
  label: 'Delete(删除)',
  text: '删除'
}, {
  value: 'Print',
  label: 'Print(打印)',
  text: '打印'
}, {
  value: 'Export',
  label: 'Export(输出)',
  text: '输出'
}, {
  value: 'Accessory',
  label: 'Accessory(附件)',
  text: '附件'
}, {
  value: 'Approve',
  label: 'Approve(审核)',
  text: '审核'
}, {
  value: 'CancelApprove',
  label: 'CancelApprove(取消审核)',
  text: '取消审核'
}, {
  value: 'Close',
  label: 'Close(结案)',
  text: '结案'
}, {
  value: 'UnClose',
  label: 'UnClose(取消结案)',
  text: '取消结案'
}, {
  value: 'Invalidate',
  label: 'Invalidate(作废)',
  text: '作废'
}, {
  value: 'UnInvalidate',
  label: 'UnInvalidate(取消作废)',
  text: '取消作废'
}, {
  value: 'DetailVaryState',
  label: 'DetailVaryState(行项状态切换)',
  text: '行项状态切换'
}]

// 功能类型
export const functionType = [{
  value: 'DataFuncPermit',
  label: 'DataFuncPermit（表单维护类）'
}, {
  value: 'GridFuncPermit',
  label: 'GridFuncPermit（表格维护类）'
}, {
  value: 'SystemFuncPermit',
  label: 'SystemFuncPermit（操作功能类）'
}, {
  value: 'ReportPermit',
  label: 'ReportPermit（功能报表类）'
}, {
  value: 'SimplePermit',
  label: 'SimplePermit（自定义权限类）'
}, {
  value: 'CustomReportPermit',
  label: 'CustomReportPermit（自设报表类)'
}]

// 数据类型
export const typeOptions = [{
  label: '未定义',
  options: [{
    value: 'Unknown',
    label: 'Unknown("未定义", 20)'
  }]
}, {
  label: '文本(非Unicode字符数据类型)',
  options: [{
    value: 'AsciiText',
    label: 'AsciiText("键值(ASCII码字符)", 20)'
  }]
}, {
  label: '文本(Unicode字符数据类型)',
  options: [{
    value: 'NText',
    label: 'NText("文本", 50)'
  }, {
    value: 'Memo',
    label: 'Memo("大文本区", 500)'
  }, {
    value: 'LongMemo',
    label: 'LongMemo("超大文本区",)'
  }, {
    value: 'RichMemo',
    label: 'RichMemo("富文本区")'
  }]
},
{
  label: '数值',
  options: [{
    value: 'Boolean',
    label: 'Boolean("是/否", 1)'
  }, {
    value: 'TinyInt',
    label: 'TinyInt("小值(-128~127)")'
  }, {
    value: 'SmallInt',
    label: 'SmallInt("小整数(–32768~32767)", 2)'
  }, {
    value: 'Integer',
    label: 'Integer("整数", 4)'
  }, {
    value: 'LongInteger',
    label: 'LongInteger("大整数", 8)'
  }, {
    value: 'Decimal',
    label: 'Decimal("精确型数值(如金额等)")'
  }, {
    value: 'Double',
    label: 'Double("浮点型数值",8)'
  }, {
    value: 'AutoNumber',
    label: 'AutoNumber("自增长值", 8)'
  }]
},
{
  label: '年月日',
  options: [{
    value: 'Date',
    label: 'Date("日期", 4)'
  }, {
    value: 'Time',
    label: 'Time("时间", 4)'
  }, {
    value: 'DateTime',
    label: 'DateTime("日期时间", 8)'
  }, {
    value: 'TimeStamp',
    label: 'TimeStamp("时间戳(TimeStamp", 8)'
  }]
}, {
  label: '特殊',
  options: [{
    value: 'GUID',
    label: 'GUID("GUID", 32)'
  }]
}
]
