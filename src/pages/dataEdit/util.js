
import { nameReg, chineseReg } from '@/assets/js/comm/constant'
export default {
  transformData: objOrigin => {
    let unquen = 1
    const obj = JSON.parse(JSON.stringify(objOrigin))
    obj.level = 1
    obj.label = obj.name
    obj.treeNodeUnquen = unquen++
    if (obj.defineDataSets) {
      obj.children = obj.defineDataSets
      for (let item of obj.children) { // 数据集
        item.level = 2
        item.label = item.name
        item.treeNodeUnquen = unquen++
        if (item.defineDBTables) {
          item.children = item.defineDBTables
          for (let son of item.children) { // 表
            son.level = 3
            son.label = son.name
            son.treeNodeUnquen = unquen++
            if (son.defineFields) {
              son.children = son.defineFields
              for (let gson of son.children) { // 字段
                gson.treeNodeUnquen = unquen++
                gson.fromRelationTable = son.tableType === 'Relative'
                gson.level = 4
                gson.label = gson.fieldName ? gson.fieldName : gson.name
                gson.name = gson.fieldName ? gson.fieldName : gson.name
                if (gson.relativeDataSources) {
                  for (let rItem of gson.relativeDataSources) {
                    rItem.label = rItem.relativeSourceName
                  }
                }
              }
            }
          }
        }
      }
    }
    if (obj.linkageMappings) { // 联动计算模型
      for (let item of obj.linkageMappings) {
        item.label = item.linkageModelName
      }
    }
    // if (obj.defaultConditionFields) { // 预设查询字段
    //   for (let item of obj.defaultConditionFields) {
    //     item.label = item.fieldName
    //   }
    // }
    if (obj.defineRptBounds) { // 报表自定义
      for (let item of obj.defineRptBounds) {
        item.label = item.name
      }
    }
    if (obj.defineRptHeadTables) { // 报表条件
      for (let item of obj.defineRptHeadTables) {
        item.label = item.fieldName
      }
    }
    return { unquen, data: [obj] }
  },
  backTransform: objOrigin => {
    const obj = JSON.parse(JSON.stringify(objOrigin))
    const appInfo = JSON.parse(localStorage.getItem('appInfo'))
    obj.appName = appInfo.name
    obj.appVersion = appInfo.appVersion
    obj.defineDataSets = obj.children || []
    if (obj.children) {
      for (let item of obj.children) {
        item.defineDBTables = item.children || []
        if (item.children) {
          for (let sonItem of item.children) {
            sonItem.label = sonItem.name
            sonItem.fieldName = sonItem.name
            if (sonItem.children && sonItem.children.length) {
              sonItem.defineFields = sonItem.children || []
            }
            delete sonItem.children
          }
          delete item.children
        }
      }
    }

    if (obj.linkageMappings) { // 联动计算模型
      for (let item of obj.linkageMappings) {
        item.linkageModelName = item.label
      }
    }
    // if (obj.defaultConditionFields) { // 预设查询字段
    //   for (let item of obj.defaultConditionFields) {
    //     item.fieldName = item.label
    //   }
    // }
    if (obj.defineRptBounds) { // 区间选项集
      for (let item of obj.defineRptBounds) {
        item.name = item.label
      }
    }
    if (obj.defineRptHeadTables) { // 表头字段集
      for (let item of obj.defineRptHeadTables) {
        item.fieldName = item.label
      }
    }
    delete obj.children
    return obj
  },
  validateAll (data, vueInstance) {
    console.log('校验', data)
    let msg = ''
    if (!data.name) {
      msg = '当前功能未输入名称代码'
    } else if (!nameReg.test(data.name)) {
      msg = '当前功能名称不符合规范'
    } else if (!data.displayName) {
      msg = '当前功能未输入中文名称'
    } else if (!chineseReg.test(data.displayName)) {
      msg = '当前功能中文名称不符合规范'
    } else if (data.children && data.children.length > 0) {
      for (let item of data.children) {
        console.log(chineseReg.test(item.displayName))
        if (!item.name) {
          msg = '您有未输入数据集代码的数据集'
          break
        } else if (!nameReg.test(item.name)) {
          msg = `数据集‘${item.name}’的名称代码不符合规范`
          break
        } else if (!item.displayName) {
          msg = `数据集‘${item.name}’未输入中文名称`
        } else if (!chineseReg.test(item.displayName)) {
          msg = `数据集‘${item.name}’中文名称不符合规范`
        } else if (item.children && item.children.length > 0) {
          item.tempTables = [] // 存所有表的表名
          item.tempMainKeys = [] // 存所有表的主键个数
          for (let son of item.children) {
            if (!son.name && son.tableType === 'Relative') {
              msg = '您有未输入名称的代码关联表'
              break
            } else if (!son.relationTableName && son.tableType === 'Relative') {
              msg = `数据集‘${item.name}’下的关联表‘${son.name}’未选择引用表名称`
              break
            } else if (!son.name) {
              msg = `数据集‘${item.name}’下有未输入名称的表`
              break
            } else if (!nameReg.test(son.name)) {
              msg = `数据集‘${item.name}’下的表‘${son.name}’的名称代码不符合规范`
              break
            } else if (son.children && son.children.length >= 3 && son.tableType !== 'Relative') {
              if (!son.primaryKeyFields) {
                item.tempMainKeys.push(0)
              } else if (son.primaryKeyFields instanceof Array) {
                const keyLength = son.primaryKeyFields.length
                item.tempMainKeys.push(keyLength)
              }
              for (let gson of son.children) {
                if (!gson.name) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中有未输入名称代码的表字段`
                } else if (!nameReg.test(gson.name)) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中字段‘${gson.name}’名称代码不符合规范`
                } else if (!gson.displayName) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中的字段‘${gson.name}’未输入中文名称`
                } else if (!chineseReg.test(gson.displayName)) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中的字段‘${gson.name}’输入中文名称不符合规范`
                } else if (!gson.dataType) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中的字段‘${gson.name}’未选择数据类型`
                } else if (!gson.fieldKind) {
                  msg = `数据集‘${item.name}’下的表‘${son.name}’中的字段‘${gson.name}’未选择字段类别`
                }
              }
              if (son.defineIndices) {
                for (let gsonIndex of son.defineIndices) {
                  if (!gsonIndex.name) {
                    msg = `数据集‘${item.name}’下的表‘${son.name}’中有未填写名称的索引`
                  } else if (!nameReg.test(gsonIndex.name)) {
                    msg = `数据集‘${item.name}’下的表‘${son.name}’中索引‘${gsonIndex.name}’的名称不符合规范`
                  } else if (!gsonIndex.fieldDetailText) {
                    msg = `数据集‘${item.name}’下的表‘${son.name}’中索引‘${gsonIndex.name}’未选择字段`
                  }
                }
              }
            } else if ((!son.children || son.children.length < 3) && son.tableType !== 'Relative') {
              msg = `数据集‘${item.name}’下的表‘${son.name}’的字段小于3个`
            }
            item.tempTables.push(son.name)
          }
        } else if (data.children.length === 1 && (!item.children || item.children.length === 0)) {
          msg = `数据集‘${item.name}中未建表`
        }
      }
    } else if (!data.children || data.children.length === 0) {
      msg = '至少有一个数据集，一张表，三个字段才可提交'
    }

    if (data.children && data.children.length > 0 && !msg) {
      for (let tmepItem of data.children) {
        for (let key in tmepItem) {
          if (key.indexOf('conFlictFileds_') > -1) {
            delete tmepItem[key]
          }
        }
        // 同一数据集下所有表的表字段不能重复
        let conflictObj = {}
        if (tmepItem.children) {
          for (let i = 0; i < tmepItem.children.length; i++) {
            if (tmepItem.children[i].tableType === 'Actual') {
              //   for (let fItem of tmepItem.children[i].children) {
              //     if (!fItem.isPrimaryKey) {
              //       conflictObj[fItem.name] = []
              //       conflictObj[fItem.name].push(tmepItem.children[i].name)
              //       conflictObj[`${fItem.name}_item`] = fItem
              //     }
              //   }
              // } else if (tmepItem.children[i].tableType === 'Actual') {
              for (let fItem of tmepItem.children[i].children) {
                if (!fItem.isPrimaryKey && conflictObj.hasOwnProperty(fItem.name)) {
                  if (!tmepItem[`conFlictFileds_${fItem.name}`]) {
                    tmepItem[`conFlictFileds_${fItem.name}`] = [conflictObj[`${fItem.name}_item`], fItem]
                  } else {
                    tmepItem[`conFlictFileds_${fItem.name}`].push(fItem)
                  }
                  conflictObj[fItem.name].push(tmepItem.children[i].name)
                  vueInstance.$set(fItem, 'isRed', true)
                  vueInstance.$set(conflictObj[`${fItem.name}_item`], 'isRed', true)
                } else if (!fItem.isPrimaryKey) {
                  conflictObj[fItem.name] = []
                  conflictObj[fItem.name].push(tmepItem.children[i].name)
                  conflictObj[`${fItem.name}_item`] = fItem
                }
              }
            }
          }
        }
        for (let key in conflictObj) {
          if (conflictObj[key].length > 1) {
            msg += msg ? '' : `<b>${tmepItem.name}</b>数据集下:<br/>`
            let str = '表'
            let obj = {}
            for (let item of conflictObj[key]) {
              if (!obj[item]) {
                obj[item] = 1
                str += `<b>${item}</b>,`
              }
            }
            str = str.substr(0, str.length - 1)
            msg += `${str}中存在重名字段<b><i>${key}</i></b><br/>`
          }
        }

        // if (tmepItem.tempFileds) {
        //   const filedsArrayAfterUniq = Array.from(new Set(tmepItem.tempFileds))
        //   if (filedsArrayAfterUniq.length !== tmepItem.tempFileds.length) {
        //     msg = `同一数据集下的表中不能含有重名字段，${tmepItem.name}数据集下的表中有重名字段`
        //   }
        // }

        // 同一数据集下表名不能重复
        if (tmepItem.tempTables) {
          const tablesArrayAfterUniq = Array.from(new Set(tmepItem.tempTables))
          if (tablesArrayAfterUniq.length !== tmepItem.tempTables.length) {
            msg = `同一数据集下表名不能重复，${tmepItem.name}数据集下有重名的表`
          }
        }

        // 同一数据集下所有表的主键个数要一致
        if (tmepItem.tempMainKeys) {
          const mainKeysArrayAfterUniq = Array.from(new Set(tmepItem.tempMainKeys))
          if (mainKeysArrayAfterUniq.length > 1) {
            msg = `同一数据集下的表的主键个数要一致，${tmepItem.name}数据集下的表主键个数不一致`
          }
        }
      }
    }
    return msg
  },
  rangeCheck (_this) {
    if (!_this.defineRptBoundsTemp.length) {
      _this.$alert('请添加成员列表')
      return false
    }
    let msg = ''
    let obj = {}
    let captionobj = {}
    for (let i = 0; i < _this.defineRptBoundsTemp.length; i++) {
      if (!_this.defineRptBoundsTemp[i].name) {
        msg = '成员列表下第' + (i + 1) + '条数据’ 名称 ‘未填写'
        break
      } else {
        if (obj[_this.defineRptBoundsTemp[i].name]) {
          obj[_this.defineRptBoundsTemp[i].name]['count'] += 1
          obj[_this.defineRptBoundsTemp[i].name]['list'].push(i + 1)
        } else {
          obj[_this.defineRptBoundsTemp[i].name] = {}
          obj[_this.defineRptBoundsTemp[i].name]['count'] = 1
          obj[_this.defineRptBoundsTemp[i].name]['list'] = [(i + 1)]
        }
      }
      if (!_this.defineRptBoundsTemp[i].caption) {
        msg = '成员列表下第' + (i + 1) + '条数据’ 标题 ‘未填写'
        break
      } else {
        if (captionobj[_this.defineRptBoundsTemp[i].caption]) {
          captionobj[_this.defineRptBoundsTemp[i].caption]['count'] += 1
          captionobj[_this.defineRptBoundsTemp[i].caption]['list'].push(i + 1)
        } else {
          captionobj[_this.defineRptBoundsTemp[i].caption] = {}
          captionobj[_this.defineRptBoundsTemp[i].caption]['count'] = 1
          captionobj[_this.defineRptBoundsTemp[i].caption]['list'] = [(i + 1)]
        }
      }
      let is = true
      for (let j = 0; j < _this.defineRptBoundsTemp[i].defineRptBoundItems.length; j++) {
        if (!_this.defineRptBoundsTemp[i].defineRptBoundItems[j].name) {
          is = false
          msg = '成员列表下第' + (i + 1) + '条数据下表格的第' + (j + 1) + '条’ 名称 ‘未填写'
          break
        }
        if (_this.defineRptBoundsTemp[i].defineRptBoundItems[j].fromValue) {
          if (!/^[0-9]*$/.test(_this.defineRptBoundsTemp[i].defineRptBoundItems[j].fromValue)) {
            is = false
            msg = '成员列表下第' + (i + 1) + '条数据下表格的第' + (j + 1) + '条’ 区间起始值 ‘请填写整数'
            break
          }
        }
        if (_this.defineRptBoundsTemp[i].defineRptBoundItems[j].toValue) {
          if (!/^[0-9]*$/.test(_this.defineRptBoundsTemp[i].defineRptBoundItems[j].toValue)) {
            is = false
            msg = '成员列表下第' + (i + 1) + '条数据下表格的第' + (j + 1) + '条’ 区间结束值 ‘请填写整数'
            break
          }
        }
      }
      if (!is) {
        break
      }
    }
    if (msg) {
      _this.$alert(msg)
      return false
    }
    let str = '成员列表下'
    let isrepeat = false
    for (let key in obj) {
      if (obj[key].count > 1) {
        isrepeat = true
        str += '第' + obj[key].list + '条数据名称重名,'
      }
    }
    if (isrepeat) {
      _this.$alert(str.substr(0, str.length - 1) + '请修改！')
      return false
    }
    let str2 = '成员列表下'
    let isrepeat2 = false
    for (let key in captionobj) {
      if (captionobj[key].count > 1) {
        isrepeat2 = true
        str2 += '第' + captionobj[key].list + '条数据标题重名,'
      }
    }
    if (isrepeat2) {
      _this.$alert(str2.substr(0, str2.length - 1) + '请修改！')
      return false
    }
    return true
  },
  headCheck (_this) {
    if (!_this.defineRptHeadTablesTemp.length) {
      _this.$alert('请添加成员列表')
      return false
    }
    let msg = ''
    let obj = {}
    for (let i = 0; i < _this.defineRptHeadTablesTemp.length; i++) {
      if (!_this.defineRptHeadTablesTemp[i].label) {
        msg = '成员列表下第' + (i + 1) + '条数据’ 字段代码 ‘未填写'
        break
      } else {
        if (obj[_this.defineRptHeadTablesTemp[i].label]) {
          obj[_this.defineRptHeadTablesTemp[i].label]['count'] += 1
          obj[_this.defineRptHeadTablesTemp[i].label]['list'].push(i + 1)
        } else {
          obj[_this.defineRptHeadTablesTemp[i].label] = {}
          obj[_this.defineRptHeadTablesTemp[i].label]['count'] = 1
          obj[_this.defineRptHeadTablesTemp[i].label]['list'] = [(i + 1)]
        }
      }
      if (!_this.defineRptHeadTablesTemp[i].dataType) {
        msg = '成员列表下第' + (i + 1) + '条数据’ 数据类型 ‘未选择'
        break
      }
    }
    if (msg) {
      _this.$alert(msg)
      return false
    }
    let str = '成员列表下'
    let isrepeat = false
    for (let key in obj) {
      if (obj[key].count > 1) {
        isrepeat = true
        str += '第' + obj[key].list + '条数据字段代码重名,'
      }
    }
    if (isrepeat) {
      _this.$alert(str.substr(0, str.length - 1) + '请修改！')
      return false
    }
    return true
  }
}
