// 基础控件
export const basicControls = [
  {
    name: 'input',
    displayName: '单行文本',
    label: '单行文本',
    icon: 'icon-danhangshurukuang',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    size: 'small', // 尺寸medium / small / mini
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组 base、rules、advanced、data
    defaultValue: '', // 默认值
    placeholder: '请填写',
    disabled: false,
    reg: {}, // 正则校验
    rules: [ // 常规校验
      { required: false, validator: null, message: '字符不得为空', trigger: 'blur' },
      { min: 2, max: 32, message: '长度不得超过32个字符', trigger: 'blur' }
    ],
    dataType: 'string'
  },
  {
    name: 'textarea',
    displayName: '多行文本',
    label: '多行文本',
    icon: 'icon-duohangshurukuang',
    rows: 2, // 占据的行数
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 100, // 控件项占据的高度
    minHeight: 90,
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    defaultValue: '',
    placeholder: '请输入文本',
    disabled: false,
    reg: {}, // 正则校验
    rules: [ // 常规校验
      { required: false, validator: null, message: '字符不得为空', trigger: 'blur' },
      { min: 2, max: 64, message: '长度不得超过64个字符', trigger: 'blur' }
    ]
  },
  {
    name: 'number',
    displayName: '计数器',
    label: '计数器',
    icon: 'icon-jisuan',
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    defaultValue: 0,
    disabled: false,
    required: false,
    min: 0,
    max: 1000,
    step: 1,
    controlsPosition: '' // 控制按钮位置 可选right
  },
  {
    name: 'radio',
    displayName: '单选框',
    label: '单选框',
    icon: 'icon-iconfontradiobox',
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    inline: true,
    defaultValue: '',
    value: '',
    disabled: false,
    showLabel: false,
    options: [
      {
        value: '1',
        label: '选项1'
      },
      {
        value: '2',
        label: '选项2'
      },
      {
        value: '3',
        label: '选项3'
      }
    ],
    required: false,
    remote: false,
    remoteOptions: [],
    props: {
      value: 'value',
      label: 'label'
    },
    remoteFunc: ''
  },
  {
    name: 'checkbox',
    displayName: '多选框',
    label: '多选框',
    icon: 'icon-duoxuankuang1',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    inline: true,
    defaultValue: [],
    value: [],
    disabled: false,
    showLabel: false,
    options: [
      {
        value: '1',
        label: '选项1'
      },
      {
        value: '2',
        label: '选项2'
      },
      {
        value: '3',
        label: '选项3'
      }
    ],
    required: false,
    remote: false,
    remoteOptions: [],
    props: {
      value: 'value',
      label: 'label'
    },
    remoteFunc: ''
  },
  {
    name: 'time',
    displayName: '时间选择器',
    label: '时间选择器',
    icon: 'icon-shijianxuanzeqi',
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    defaultValue: '21:19:56',
    readonly: false,
    disabled: false,
    editable: true, // 文本框可否输入
    clearable: true, // 是否显示清除按钮
    placeholder: '',
    startPlaceholder: '', // 范围选择时开始日期的占位内容
    endPlaceholder: '', // 范围选择时结束日期的占位内容
    isRange: false, // 是否为时间范围选择
    arrowControl: true,
    format: 'HH:mm:ss',
    required: false
  },
  {
    name: 'date',
    displayName: '日期选择器',
    label: '日期选择器',
    icon: 'icon-riqi',
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    defaultValue: '',
    readonly: false,
    disabled: false,
    editable: true,
    clearable: true,
    placeholder: '',
    startPlaceholder: '',
    endPlaceholder: '',
    type: 'date',
    format: 'yyyy-MM-dd',
    timestamp: false,
    required: false
  },
  {
    name: 'rate',
    displayName: '评分',
    label: '评分',
    icon: 'icon-pingfen',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    defaultValue: null,
    max: 5,
    disabled: false,
    allowHalf: false,
    required: false
  },
  {
    name: 'color',
    displayName: '颜色选择器',
    label: '颜色选择器',
    icon: 'icon-yanse',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    defaultValue: '',
    disabled: false,
    showAlpha: false,
    required: false
  },
  {
    name: 'select',
    displayName: '下拉选择框',
    label: '下拉选择框',
    icon: 'icon-xiala2',
    span: 24,
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0,
    push: 0,
    pull: 0,
    openConfigs: ['base', 'rules', 'advanced', 'data'],
    defaultValue: '',
    multiple: false,
    disabled: false,
    clearable: false, // 可清空单选
    placeholder: '',
    required: false,
    showLabel: false,
    options: [
      {
        value: '1',
        label: '选项1'
      },
      {
        value: '2',
        label: '选项2'
      },
      {
        value: '3',
        label: '选项3'
      }
    ],
    remote: false,
    remoteOptions: [],
    props: {
      value: 'value',
      label: 'label'
    },
    remoteFunc: ''
  },
  {
    name: 'switch',
    displayName: '开关',
    label: '开关',
    icon: 'icon-39kaiguankai2',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    defaultValue: false,
    disabled: false,
    required: false
  },
  {
    name: 'slider',
    displayName: '滑块',
    label: '滑块',
    icon: 'icon-huakuai',
    span: 24, // 栅格占据的列数
    width: 100, // 占据父元素的宽度百分比
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    defaultValue: 0,
    disabled: false,
    required: false,
    min: 0,
    max: 100,
    step: 1,
    showInput: false,
    range: false, // 是否为范围选择
    style: { width: '100%' }
  }
]
// 高级控件
export const advancedControls = [
  {
    name: 'button',
    displayName: '按钮',
    label: '按钮',
    icon: 'icon-anniu2',
    span: 24,
    width: 10, // 占据父元素的宽度百分比例
    offset: 0, // 左侧间隔宽度百分比例
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    disabled: false,
    type: 'primary',
    size: 'small', // medium、small、mini
    action: '',
    form: ''
  },
  {
    name: 'text',
    displayName: '文字',
    label: '文字',
    icon: 'icon-wenben1',
    value: '默认文本', // 文本内容
    span: 24, // 栅格占据的列数
    width: 10, // 占据父元素的宽度百分比例
    height: 60, // 控件项占据的高度
    rows: 1, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    fontSize: '16'

  },
  // {
  //   name: 'tree',
  //   displayName: '树形控件',
  //   label: '树控件',
  //   labelWidth: 0,
  //   icon: 'icon-shuxingjiegou',
  //   span: 24, // 栅格占据的列数
  //   width: 100,
  //   height: 90, // 控件项占据的高度
  //   minHeight: 90,
  //   offset: 0, // 左侧的间隔百分比
  //   push: 0, // 栅格向右移动格数
  //   pull: 0, // 栅格向左移动格数
  //   expandAll: true, // 默认展开所有
  //   openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
  //   dataSource: '',
  //   data: [ // 控件数据
  //     {
  //       label: '一级 1',
  //       id: 1,
  //       children: [
  //         {
  //           label: '二级 1-1',
  //           id: 2,
  //           children: [
  //             {
  //               label: '三级 1-1-1',
  //               id: 3
  //             }
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: '一级 2',
  //       id: 4,
  //       children: [
  //         {
  //           label: '二级 2-1',
  //           id: 5,
  //           children: [
  //             {
  //               label: '三级 2-1-1',
  //               id: 6
  //             }
  //           ]
  //         },
  //         {
  //           label: '二级 2-2',
  //           id: 7,
  //           children: [
  //             {
  //               label: '三级 2-2-1',
  //               id: 8
  //             }
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       label: '一级 3',
  //       id: 9,
  //       children: [
  //         {
  //           label: '二级 3-1',
  //           id: 10,
  //           children: [
  //             {
  //               label: '三级 3-1-1',
  //               id: 11
  //             }
  //           ]
  //         },
  //         {
  //           label: '二级 3-2',
  //           id: 12,
  //           children: [
  //             {
  //               label: '三级 3-2-1',
  //               id: 13
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   ],
  //   props: {
  //     // 树的配置选项
  //     children: 'children',
  //     label: 'label'
  //   }
  // },
  {
    name: 'table',
    displayName: '表格',
    label: '表格',
    labelWidth: 0,
    icon: 'icon-biaoge1',
    span: 24, // 栅格占据的列数
    width: 100,
    height: 260, // 控件项占据的高度
    minHeight: 260,
    // rows: 5, // 占据的行数
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    dataSource: '',
    data: [
      {
        // 表格数据
        date: '2016-05-02',
        birthday: '2018-11-29',
        name: '王小虎',
        address: '上海市普陀区金沙江路 1518 弄A幢402室'
      },
      {
        date: '2016-05-04',
        birthday: '2018-11-29',
        name: '王小虎',
        address: '上海市普陀区金沙江路 1517 弄123室'
      },
      {
        date: '2016-05-01',
        birthday: '2018-11-29',
        name: '王小虎',
        address: '上海市普陀区金沙江路 1519 弄123室'
      },
      {
        date: '2016-05-03',
        birthday: '2018-11-29',
        name: '王小虎',
        address: '上海市普陀区金沙江路 1516 弄'
      }
    ],
    // 列的数据
    columns: [
      {
        value: 'name',
        label: '姓名',
        width: 200
      },
      {
        value: 'birthday',
        label: '出生日期',
        width: 200
      },
      {
        value: 'date',
        label: '日期',
        width: 200
      },
      {
        value: 'address',
        label: '地址',
        tooltip: true
      }
    ],
    columnList: [
      {
        name: 'name',
        displayName: '姓名',
        label: '姓名(name)',
        id: 1
      },
      {
        name: 'birthday',
        displayName: '出生日期',
        label: '出生日期(birthday)',
        id: 2
      },
      {
        name: 'date',
        displayName: '日期',
        label: '日期(date)',
        id: 3
      },
      {
        name: 'address',
        displayName: '地址',
        label: '地址(address)',
        id: 4
      }
    ],
    border: true
  },
  {
    name: 'img',
    displayName: '图片上传',
    label: '图片上传',
    icon: 'icon-icon--',
    span: 24, // 栅格占据的列数
    width: 100,
    height: 112, // 控件项占据的高度
    minHeight: 100,
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    defaultValue: [],
    disabled: false,
    size: {
      width: 100,
      height: 100
    },
    tokenFunc: 'funcGetToken',
    token: '123',
    domain: 'http://pfp81ptt6.bkt.clouddn.com/',
    limit: 1, // 上传个数限制
    multiple: true
  },
  {
    name: 'richText',
    displayName: '富文本',
    label: '富文本',
    icon: 'icon-fuwenben',
    span: 24, // 栅格占据的列数
    height: 220, // 控件项占据的高度
    minHeight: 220,
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
    value: '',
    placeholder: '',
    disabled: false,
    defaultType: 'String'
  }
  // {
  //   name: 'tabs',
  //   displayName: '标签页',
  //   label: '标签页',
  //   icon: 'icon-xuanxiangqia-',
  //   span: 24, // 栅格占据的列数
  //   height: 100, // 控件项占据的高度
  //   minHeight: 100,
  //   offset: 0, // 左侧的间隔百分比
  //   push: 0, // 栅格向右移动格数
  //   pull: 0, // 栅格向左移动格数
  //   openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
  //   tabs: [
  //     {
  //       index: '1',
  //       label: '页签1',
  //       children: []
  //     },
  //     {
  //       index: '2',
  //       label: '页签2',
  //       children: []
  //     },
  //     {
  //       index: '3',
  //       label: '页签3',
  //       children: []
  //     }
  //   ]
  // }
]
// 布局控件
export const layoutControls = [
  {
    name: 'formArea',
    displayName: '表单容器',
    label: '表单容器', // 主题
    titlePosition: 'center', // 主题位置
    allowFolding: false, // 允许折叠
    haveAnchor: false, // 添加锚点
    isFolding: false, // 是否折叠
    icon: 'icon-8636f874',
    span: 24, // 栅格占据的列数
    columns: 1, // 区块分成几份
    rows: 1, // 占据的行数
    height: 80, // 控件项占据的高度
    gutterV: 5, // 控件上下间隔，单位px
    gutterH: 10, // 控件左右间隔，单位px
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    openConfigs: ['base', 'layout', 'rules', 'advanced', 'data'], // 展开的配置组
    children: [],
    justify: 'start', // flex布局下水平排列方式start/end/center/space-around/space-between
    align: 'top', // flex布局下垂直排列方式top/middle/bottom
    labelWidth: 90, // label宽度
    labelPosition: 'left', // label的位置
    model: {}, // 表单数据对象
    rules: {}, // 数据校验对象
    selectFields: [], // 已选快速生成控件的字段的列表
    customStyle: '' // 自定义样式
  },
  {
    name: 'baseArea',
    displayName: '布局容器',
    label: '布局容器', // 主题
    titlePosition: 'center', // 主题位置
    allowFolding: false, // 允许折叠
    haveAnchor: false, // 添加锚点
    isFolding: false, // 是否折叠
    icon: 'icon-jiugongge',
    span: 24, // 栅格占据的列数
    columns: 1, // 区块分成几份
    rows: 1, // 占据的行数
    height: 80, // 控件项占据的高度
    gutterV: 5, // 控件上下间隔，单位px
    gutterH: 10, // 控件左右间隔，单位px
    offset: 0, // 左侧的间隔百分比
    push: 0, // 栅格向右移动格数
    pull: 0, // 栅格向左移动格数
    labelWidth: 90, // 控件label宽度
    labelPosition: 'left', // 控件label的位置
    openConfigs: ['base', 'layout', 'rules', 'advanced', 'data'], // 展开的配置组
    children: [],
    selectFields: [], // 已选快速生成控件的字段的列表
    justify: 'start',
    align: 'top'
  }
  // {
  //   name: 'tabArea',
  //   displayName: '标签页容器',
  //   label: '标签页容器',
  //   icon: 'icon-xuanxiangqia',
  //   span: 24, // 栅格占据的列数
  //   height: 200, // 控件项占据的高度
  //   gutter: 10, // 栅格间隔，单位px
  //   marginV: 10, // 上下边距，单位px 竖:vertical
  //   marginH: 10, // 左右边距，单位px 横:horizontal
  //   offset: 0, // 左侧的间隔百分比
  //   push: 0, // 栅格向右移动格数
  //   pull: 0, // 栅格向左移动格数
  //   openConfigs: ['base', 'rules', 'advanced', 'data'], // 展开的配置组
  //   children: [],
  //   justify: 'start',
  //   align: 'top',
  //   tabs: [
  //     {
  //       index: '1',
  //       label: '页签1',
  //       children: []
  //     },
  //     {
  //       index: '2',
  //       label: '页签2',
  //       children: []
  //     },
  //     {
  //       index: '3',
  //       label: '页签3',
  //       children: []
  //     }
  //   ]
  // }
]
/** 通过字段的控件类型和实际控件映射关系，获取控件数据
 *@param {String} type：字段控件类型
 *@description  由于数据模型给的字段的控件类型和实际控件类型不匹配，所以要有个映射关系对应起来
 *@return {Object} 得到的控件数据
 *@author guotongqing
 */
export function getControl (type) {
  let controlList = basicControls.concat(advancedControls)
  let obj = {}
  controlList.map(item => {
    obj[item.name] = item
  })
  switch (type) {
    case 'Memo' || 'LongMemo': return obj.textarea
    case 'RichMemo': return obj.richText
    case 'TextOption': return obj.radio
    case 'MultiOption': return obj.checkbox
    case 'Numeric' || 'SpinEdit': return obj.number
    case 'Rate': return obj.rate
    case 'Date': return obj.date
    case 'Time': return obj.time
    case 'Image': return obj.img
    case 'ColorEdit': return obj.color
    default: return obj.input
  }
}
// 树控件模拟数据
export const treeData = [
  {
    label: '组织结构',
    list: [
      {
        label: '总部',
        id: 1,
        children: [
          {
            label: '整车销售部',
            id: 2,
            children: [
              {
                label: '门店销售一组',
                id: 3
              },
              {
                label: '门店销售二组',
                id: 4
              },
              {
                label: '门店销售三组',
                id: 5
              },
              {
                label: '门店销售四组',
                id: 6
              }
            ]
          },
          {
            label: '售后管理部',
            id: 7,
            children: [
              {
                label: '子部门',
                id: 8
              }
            ]
          },
          {
            label: '客户服务部',
            id: 9,
            children: [
              {
                label: '子部门',
                id: 10
              }
            ]
          },
          {
            label: '技术支持部',
            id: 11,
            children: [
              {
                label: '子部门',
                id: 12
              }
            ]
          }
        ]
      }
    ]
  }
]
